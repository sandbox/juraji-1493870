/*
  Document   : content_translation_progress_block.js
  Created on : 21-mrt-2012, 21:58:41
  Author     : Robin Kesseler
  Description:
  Content translation progress block JQuery additions
 */

function updateCSS(caller) {
  console.log('Content Translation: updateCSS called for: '+caller);
  if (caller == 'edit-content-translation-progress-block-show-progressbar' || caller == 'all'){
    if ($('#edit-content-translation-progress-block-show-progressbar').attr('checked')){
      $('#edit-content-translation-progress-block-show-progressbar-labels-wrapper').fadeIn('slow', function() {
        $(this).css('display', null);
      });
    }else{
      $('#edit-content-translation-progress-block-show-progressbar-labels-wrapper').fadeOut('slow', function() {
        $(this).css('display', 'none');
      });
    }
  }
  if (caller == 'edit-content-translation-progress-block-show-no-translation-list' || caller == 'all'){
    if ($('#edit-content-translation-progress-block-show-no-translation-list').attr('checked')){
      $('#edit-content-translation-progress-block-show-no-translation-list-user-wrapper').fadeIn('slow', function() {
        $(this).css('display', null);
      });
    }else{
      $('#edit-content-translation-progress-block-show-no-translation-list-user-wrapper').fadeOut('slow', function() {
        $(this).css('display', 'none');
      });
    }
  }
}

$(function (){
  if($('#content-translation-progress-block-admin')){
    $('#edit-content-translation-progress-block-show-progressbar').click(function (){
      updateCSS($(this).attr('id'));
    });
    $('#edit-content-translation-progress-block-show-no-translation-list').click(function (){
      updateCSS($(this).attr('id'));
    });
  }
});

$(document).ready(function (){
  if($('#content-translation-progress-block-admin')){
    updateCSS('all');
  }
});